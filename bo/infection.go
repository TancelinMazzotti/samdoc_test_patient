package bo

import "time"

type Infection struct {
	Date_infection time.Time
	Patient        Patient
}
