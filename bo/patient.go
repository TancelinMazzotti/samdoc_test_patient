package bo

import "time"

type Patient struct {
	Id_patient uint32
	Nom        string
	Prenom     string
	Civilite   string
	Naissance  time.Time
}
