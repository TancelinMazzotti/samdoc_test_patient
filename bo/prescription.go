package bo

import "time"

type Prescription struct {
	Id_prescription uint32
	Patient         Patient
	Maladie         Maladie
	Date_infection  time.Time
	Medicaments     []Medicament
	Detail          string
}
