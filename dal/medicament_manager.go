package dal

import (
	"database/sql"
	"samdoc_test_patient/bo"
)

func SelectAllMedicaments() ([]bo.Medicament, bool) {
	db := GetInstanceDB()
	medicaments := make([]bo.Medicament, 0)
	success := true

	sqlStatement := `SELECT id_medicament, libelle, description FROM medicament ORDER BY libelle;`

	rows, err := db.Query(sqlStatement)
	if err != nil {
		success = false
		panic(err)
	}
	for rows.Next() {
		var id_medicament uint32
		var libelle string
		var description sql.NullString

		if err := rows.Scan(&id_medicament, &libelle, &description); err != nil {
			success = false
			panic(err)
		}
		medicament := bo.Medicament{}
		medicament.Id_medicament = id_medicament
		medicament.Libelle = libelle
		medicament.Description = description.String
		medicaments = append(medicaments, medicament)
	}

	return medicaments, success
}

func SelectMedicament(id_medicament uint32) (bo.Medicament, bool) {
	db := GetInstanceDB()
	medicament := bo.Medicament{}
	success := true

	sqlStatement := `SELECT libelle, description FROM medicament
	WHERE id_medicament=$1;`

	var libelle string
	var description sql.NullString

	row := db.QueryRow(sqlStatement, id_medicament)

	if err := row.Scan(&libelle, &description); err != nil {
		success = false
		panic(err)
	} else {
		medicament.Id_medicament = id_medicament
		medicament.Libelle = libelle
		medicament.Description = description.String
	}

	return medicament, success
}

func SelectMedicamentsByPrescription(id_prescription uint32) ([]bo.Medicament, bool) {
	db := GetInstanceDB()
	medicaments := make([]bo.Medicament, 0)
	success := true

	sqlStatement := `SELECT id_medicament, libelle, description FROM medicament
	INNER JOIN line_prescription USING (id_medicament)
	WHERE id_prescription=$1
	ORDER BY libelle;`

	rows, err := db.Query(sqlStatement, id_prescription)
	if err != nil {
		success = false
		panic(err)
	}
	for rows.Next() {
		var id_medicament uint32
		var libelle string
		var description sql.NullString

		if err := rows.Scan(&id_medicament, &libelle, &description); err != nil {
			success = false
			panic(err)
		}
		medicament := bo.Medicament{}
		medicament.Id_medicament = id_medicament
		medicament.Libelle = libelle
		medicament.Description = description.String
		medicaments = append(medicaments, medicament)
	}

	return medicaments, success
}
