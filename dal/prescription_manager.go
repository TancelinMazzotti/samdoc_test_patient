package dal

import (
	"database/sql"
	"samdoc_test_patient/bo"
	"time"
)

func SelectAllPrecriptions() ([]bo.Prescription, bool) {
	db := GetInstanceDB()
	prescriptions := make([]bo.Prescription, 0)
	success := true

	sqlStatement := `SELECT id_prescription, id_patient, id_maladie, 
	date_infection, detail FROM prescription ORDER BY date_infection;`

	rows, err := db.Query(sqlStatement)
	if err != nil {
		success = false
		panic(err)
	}
	for rows.Next() {
		var id_prescription uint32
		var id_patient uint32
		var id_maladie uint32
		var date_infection time.Time
		var detail sql.NullString

		if err := rows.Scan(&id_prescription, &id_patient, &id_maladie, &date_infection, &detail); err != nil {
			success = false
			panic(err)
		}

		var patient, _ = SelectPatient(id_patient)
		var maladie, _ = SelectMaladie(id_maladie)
		var medicaments, _ = SelectMedicamentsByPrescription(id_prescription)

		prescription := bo.Prescription{}
		prescription.Id_prescription = id_prescription
		prescription.Patient = patient
		prescription.Maladie = maladie
		prescription.Medicaments = medicaments
		prescription.Date_infection = date_infection
		prescription.Detail = detail.String
		prescriptions = append(prescriptions, prescription)

	}

	return prescriptions, success
}

func SelectPrescription(id_prescription uint32) (bo.Prescription, bool) {
	db := GetInstanceDB()
	prescription := bo.Prescription{}
	success := true

	sqlStatement := `SELECT id_patient, id_maladie, 
	date_infection, detail FROM prescription WHERE id_prescription=$1;`

	var id_patient uint32
	var id_maladie uint32
	var date_infection time.Time
	var detail sql.NullString

	row := db.QueryRow(sqlStatement, id_prescription)

	if err := row.Scan(&id_patient, &id_maladie, &date_infection, &detail); err != nil {
		success = false
		panic(err)
	} else {
		var patient, _ = SelectPatient(id_patient)
		var maladie, _ = SelectMaladie(id_maladie)
		var medicaments, _ = SelectMedicamentsByPrescription(id_prescription)

		prescription.Id_prescription = id_prescription
		prescription.Patient = patient
		prescription.Maladie = maladie
		prescription.Medicaments = medicaments
		prescription.Date_infection = date_infection
		prescription.Detail = detail.String
	}

	return prescription, success
}

func SelectPrescriptionsByPatient(id_patient uint32) ([]bo.Prescription, bool) {
	db := GetInstanceDB()
	prescriptions := make([]bo.Prescription, 0)
	success := true

	sqlStatement := `SELECT id_prescription, id_maladie, 
	date_infection, detail FROM prescription
	WHERE id_patient=$1
	ORDER BY date_infection;`

	rows, err := db.Query(sqlStatement, id_patient)
	if err != nil {
		success = false
		panic(err)
	}
	for rows.Next() {
		var id_prescription uint32
		var id_maladie uint32
		var date_infection time.Time
		var detail sql.NullString

		if err := rows.Scan(&id_prescription, &id_maladie, &date_infection, &detail); err != nil {
			success = false
			panic(err)
		}

		var patient, _ = SelectPatient(id_patient)
		var maladie, _ = SelectMaladie(id_maladie)
		var medicaments, _ = SelectMedicamentsByPrescription(id_prescription)

		prescription := bo.Prescription{}
		prescription.Id_prescription = id_prescription
		prescription.Patient = patient
		prescription.Maladie = maladie
		prescription.Medicaments = medicaments
		prescription.Date_infection = date_infection
		prescription.Detail = detail.String
		prescriptions = append(prescriptions, prescription)

	}

	return prescriptions, success
}

func SelectPrescriptionsByMaladie(id_maladie uint32) ([]bo.Prescription, bool) {
	db := GetInstanceDB()
	prescriptions := make([]bo.Prescription, 0)
	success := true

	sqlStatement := `SELECT id_prescription, id_patient, 
	date_infection, detail FROM prescription
	WHERE id_maladie=$1
	ORDER BY date_infection;`

	rows, err := db.Query(sqlStatement, id_maladie)
	if err != nil {
		success = false
		panic(err)
	}
	for rows.Next() {
		var id_prescription uint32
		var id_patient uint32
		var date_infection time.Time
		var detail sql.NullString

		if err := rows.Scan(&id_prescription, &id_patient, &date_infection, &detail); err != nil {
			success = false
			panic(err)
		}

		var patient, _ = SelectPatient(id_patient)
		var maladie, _ = SelectMaladie(id_maladie)
		var medicaments, _ = SelectMedicamentsByPrescription(id_prescription)

		prescription := bo.Prescription{}
		prescription.Id_prescription = id_prescription
		prescription.Patient = patient
		prescription.Maladie = maladie
		prescription.Medicaments = medicaments
		prescription.Date_infection = date_infection
		prescription.Detail = detail.String
		prescriptions = append(prescriptions, prescription)

	}

	return prescriptions, success
}

func SelectPrescriptionsByPatientAndMaladie(id_patient uint32, id_maladie uint32) ([]bo.Prescription, bool) {
	db := GetInstanceDB()
	prescriptions := make([]bo.Prescription, 0)
	success := true

	sqlStatement := `SELECT id_prescription, 
	date_infection, detail FROM prescription
	WHERE id_patient=$1 AND id_maladie=$2
	ORDER BY date_infection;`

	rows, err := db.Query(sqlStatement, id_patient, id_maladie)
	if err != nil {
		success = false
		panic(err)
	}
	for rows.Next() {
		var id_prescription uint32
		var date_infection time.Time
		var detail sql.NullString

		if err := rows.Scan(&id_prescription, &date_infection, &detail); err != nil {
			success = false
			panic(err)
		}

		var patient, _ = SelectPatient(id_patient)
		var maladie, _ = SelectMaladie(id_maladie)
		var medicaments, _ = SelectMedicamentsByPrescription(id_prescription)

		prescription := bo.Prescription{}
		prescription.Id_prescription = id_prescription
		prescription.Patient = patient
		prescription.Maladie = maladie
		prescription.Medicaments = medicaments
		prescription.Date_infection = date_infection
		prescription.Detail = detail.String
		prescriptions = append(prescriptions, prescription)

	}

	return prescriptions, success
}
