package dal

import (
	"database/sql"
	"samdoc_test_patient/bo"
	"time"
)

func SelectAllMaladies() ([]bo.Maladie, bool) {
	db := GetInstanceDB()
	maladies := make([]bo.Maladie, 0)
	success := true

	sqlStatement := `SELECT id_maladie, libelle, description FROM maladie ORDER BY libelle;`

	rows, err := db.Query(sqlStatement)
	if err != nil {
		success = false
		panic(err)
	}
	for rows.Next() {
		var id_maladie uint32
		var libelle string
		var description sql.NullString

		if err := rows.Scan(&id_maladie, &libelle, &description); err != nil {
			success = false
			panic(err)
		}
		maladie := bo.Maladie{}
		maladie.Id_maladie = id_maladie
		maladie.Libelle = libelle
		maladie.Description = description.String
		maladies = append(maladies, maladie)
	}

	return maladies, success
}

func SelectMaladie(id_maladie uint32) (bo.Maladie, bool) {
	db := GetInstanceDB()
	maladie := bo.Maladie{}
	success := true

	sqlStatement := `SELECT libelle, description FROM maladie
	WHERE id_maladie=$1;`

	var libelle string
	var description sql.NullString

	row := db.QueryRow(sqlStatement, id_maladie)

	if err := row.Scan(&libelle, &description); err != nil {
		success = false
		panic(err)
	} else {
		maladie.Id_maladie = id_maladie
		maladie.Libelle = libelle
		maladie.Description = description.String
	}

	return maladie, success
}

func SelectMaladiesByPatient(id_patient uint32) ([]bo.Historique, bool) {
	db := GetInstanceDB()
	historiques := make([]bo.Historique, 0)
	success := true

	sqlStatement := `SELECT id_maladie, libelle, description, date_infection FROM maladie
	INNER JOIN historique USING (id_maladie)
	INNER JOIN patient USING (id_patient)
	WHERE id_patient=$1
	ORDER BY date_infection;`

	rows, err := db.Query(sqlStatement, id_patient)
	if err != nil {
		success = false
		panic(err)
	}
	for rows.Next() {
		var id_maladie uint32
		var libelle string
		var description sql.NullString
		var date_infection time.Time

		if err := rows.Scan(&id_maladie, &libelle, &description, &date_infection); err != nil {
			success = false
			panic(err)
		} else {
			maladie := bo.Maladie{}
			maladie.Id_maladie = id_maladie
			maladie.Libelle = libelle
			maladie.Description = description.String

			historique := bo.Historique{}
			historique.Date_infection = date_infection
			historique.Maladie = maladie
			historiques = append(historiques, historique)
		}

	}

	return historiques, success
}
