package dal

import (
	"samdoc_test_patient/bo"
	"time"
)

func SelectAllPatients() ([]bo.Patient, bool) {
	db := GetInstanceDB()
	patients := make([]bo.Patient, 0)
	success := true

	sqlStatement := `SELECT id_patient, nom, prenom, naissance, civilite.libelle FROM patient
	INNER JOIN civilite USING (id_civilite) ORDER BY nom, prenom;`

	rows, err := db.Query(sqlStatement)
	if err != nil {
		success = false
		panic(err)
	}
	for rows.Next() {
		var id_patient uint32
		var nom string
		var prenom string
		var civilite string
		var naissance time.Time

		if err := rows.Scan(&id_patient, &nom, &prenom, &naissance, &civilite); err != nil {
			success = false
			panic(err)
		}
		patient := bo.Patient{}
		patient.Id_patient = id_patient
		patient.Nom = nom
		patient.Prenom = prenom
		patient.Naissance = naissance
		patient.Civilite = civilite
		patients = append(patients, patient)
	}

	return patients, success
}

func SelectPatient(id_patient uint32) (bo.Patient, bool) {
	db := GetInstanceDB()
	patient := bo.Patient{}
	success := true

	sqlStatement := `SELECT nom, prenom, naissance, civilite.libelle FROM patient
	INNER JOIN civilite USING (id_civilite) WHERE id_patient=$1;`

	var nom string
	var prenom string
	var civilite string
	var naissance time.Time

	row := db.QueryRow(sqlStatement, id_patient)

	if err := row.Scan(&nom, &prenom, &naissance, &civilite); err != nil {
		success = false
	} else {
		patient.Id_patient = id_patient
		patient.Nom = nom
		patient.Prenom = prenom
		patient.Naissance = naissance
		patient.Civilite = civilite
	}

	return patient, success
}

func SelectPatientsByMaladie(id_maladie uint32) ([]bo.Infection, bool) {
	db := GetInstanceDB()
	infections := []bo.Infection{}
	success := true

	sqlStatement := `SELECT date_infection, id_patient, nom, prenom, naissance, civilite.libelle FROM patient
	INNER JOIN civilite USING (id_civilite)
	INNER JOIN historique USING (id_patient)
	WHERE id_maladie=$1
	ORDER BY date_infection;`

	rows, err := db.Query(sqlStatement, id_maladie)
	if err != nil {
		success = false
		panic(err)
	}
	for rows.Next() {
		var date_infection time.Time
		var id_patient uint32
		var nom string
		var prenom string
		var civilite string
		var naissance time.Time

		if err := rows.Scan(&date_infection, &id_patient, &nom, &prenom, &naissance, &civilite); err != nil {
			success = false
			panic(err)
		}
		patient := bo.Patient{}
		patient.Id_patient = id_patient
		patient.Nom = nom
		patient.Prenom = prenom
		patient.Naissance = naissance
		patient.Civilite = civilite

		infection := bo.Infection{}
		infection.Date_infection = date_infection
		infection.Patient = patient
		infections = append(infections, infection)
	}

	return infections, success
}

func InsertPatient(patient *bo.Patient, id_civilite uint32) bool {
	db := GetInstanceDB()
	success := true

	sqlStatement := `SELECT civilite.libelle FROM civilite
	WHERE id_civilite=$1;`

	insertSqlStatement := `INSERT INTO patient(nom, prenom, id_civilite, naissance)
	VALUES($1, $2, $3, $4) RETURNING id_patient;`

	var civilite string

	row := db.QueryRow(sqlStatement, id_civilite)

	if err := row.Scan(&civilite); err != nil {
		success = false
	} else {
		patient.Civilite = civilite
		var id_patient uint32 = 0
		err_insert := db.QueryRow(insertSqlStatement, patient.Nom, patient.Prenom, id_civilite, patient.Naissance).Scan(&id_patient)
		if err_insert != nil {
			success = false
		} else {
			patient.Id_patient = id_patient
		}

	}

	return success
}
