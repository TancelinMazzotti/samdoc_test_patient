package bll

import (
	"net/http"
	"regexp"
	"samdoc_test_patient/dal"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetAllPrescriptions(c *gin.Context) {
	prescriptions, success := dal.SelectAllPrecriptions()
	if success {
		c.JSON(200, prescriptions)

	} else {
		c.Writer.WriteHeader(http.StatusNoContent)
	}
}

func GetPrescription(c *gin.Context) {
	param_id := c.Param("id")
	id_prescription, err := strconv.ParseInt(param_id, 10, 32)
	if err == nil {
		prescription, success := dal.SelectPrescription(uint32(id_prescription))
		if success {
			c.JSON(200, prescription)
		} else {
			c.Writer.WriteHeader(http.StatusNoContent)
		}

	}
}

func GetPrescriptionByMaladieAndPatient(c *gin.Context) {
	param_id_patient := c.Param("idPatient")
	param_id_maladie := c.Param("idMaladie")

	param_id_maladie = string(regexp.MustCompile(`\/`).ReplaceAll([]byte(param_id_maladie), []byte("")))

	id_patient, err_patient := strconv.ParseInt(param_id_patient, 10, 32)
	id_maladie, err_maladie := strconv.ParseInt(param_id_maladie, 10, 32)

	if err_patient == nil && err_maladie == nil {
		prescriptions, success := dal.SelectPrescriptionsByPatientAndMaladie(uint32(id_patient), uint32(id_maladie))
		if success {
			c.JSON(200, prescriptions)
		} else {
			c.Writer.WriteHeader(http.StatusNoContent)
		}
	}
}
