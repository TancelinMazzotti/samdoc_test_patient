package bll

import (
	"fmt"
	"net/http"
	"samdoc_test_patient/bo"
	"samdoc_test_patient/dal"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

func GetAllPatients(c *gin.Context) {
	patients, success := dal.SelectAllPatients()
	if success {
		c.JSON(200, patients)

	} else {
		c.Writer.WriteHeader(http.StatusNoContent)
	}
}

func GetPatient(c *gin.Context) {
	param_id := c.Param("id")
	id_patient, err := strconv.ParseInt(param_id, 10, 32)
	if err == nil {
		patient, success := dal.SelectPatient(uint32(id_patient))
		if success {
			c.JSON(200, patient)
		} else {
			c.Writer.WriteHeader(http.StatusNoContent)
		}

	}
}

func GetMaladiesByPatient(c *gin.Context) {
	param_id := c.Param("id")
	id_patient, err := strconv.ParseInt(param_id, 10, 32)
	if err == nil {
		historiques, success := dal.SelectMaladiesByPatient(uint32(id_patient))
		if success {
			c.JSON(200, historiques)
		} else {
			c.Writer.WriteHeader(http.StatusNoContent)
		}
	}
}

func GetPrescriptionsByPatient(c *gin.Context) {
	param_id := c.Param("id")
	id_patient, err := strconv.ParseInt(param_id, 10, 32)
	if err == nil {
		prescriptions, success := dal.SelectPrescriptionsByPatient(uint32(id_patient))
		if success {
			c.JSON(200, prescriptions)
		} else {
			c.Writer.WriteHeader(http.StatusNoContent)
		}
	}
}

func SavePatient(c *gin.Context) {
	nom := c.PostForm("nom")
	prenom := c.PostForm("prenom")
	id_civilite, err_id_civilite := strconv.ParseInt(c.PostForm("id_civilite"), 10, 32)
	naissance, err_naissance := time.Parse("02/01/2006", c.PostForm("naissance"))
	if err_id_civilite == nil && err_naissance == nil {
		patient := bo.Patient{}
		patient.Nom = nom
		patient.Prenom = prenom
		patient.Naissance = naissance
		if dal.InsertPatient(&patient, uint32(id_civilite)) {
			c.JSON(200, patient)
		} else {
			c.Writer.WriteHeader(http.StatusNoContent)
		}

		fmt.Println(patient, " - ", id_civilite)
	} else {
		panic(err_naissance)
	}
}
