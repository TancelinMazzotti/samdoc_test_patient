package bll

import (
	"net/http"
	"samdoc_test_patient/dal"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetAllMedicaments(c *gin.Context) {
	medicaments, success := dal.SelectAllMedicaments()
	if success {
		c.JSON(200, medicaments)

	} else {
		c.Writer.WriteHeader(http.StatusNoContent)
	}
}

func GetMedicament(c *gin.Context) {
	param_id := c.Param("id")
	id_medicament, err := strconv.ParseInt(param_id, 10, 32)
	if err == nil {
		medicament, success := dal.SelectMedicament(uint32(id_medicament))
		if success {
			c.JSON(200, medicament)
		} else {
			c.Writer.WriteHeader(http.StatusNoContent)
		}

	}
}
