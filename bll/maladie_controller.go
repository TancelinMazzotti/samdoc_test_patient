package bll

import (
	"net/http"
	"samdoc_test_patient/dal"
	"strconv"

	"github.com/gin-gonic/gin"
)

func GetAllMaladies(c *gin.Context) {
	maladies, success := dal.SelectAllMaladies()
	if success {
		c.JSON(200, maladies)

	} else {
		c.Writer.WriteHeader(http.StatusNoContent)
	}
}

func GetMaladie(c *gin.Context) {
	param_id := c.Param("id")
	id_maladie, err := strconv.ParseInt(param_id, 10, 32)
	if err == nil {
		maladie, success := dal.SelectMaladie(uint32(id_maladie))
		if success {
			c.JSON(200, maladie)
		} else {
			c.Writer.WriteHeader(http.StatusNoContent)
		}

	}
}

func GetPatientsByMaladie(c *gin.Context) {
	param_id := c.Param("id")
	id_maladie, err := strconv.ParseInt(param_id, 10, 32)
	if err == nil {
		infections, success := dal.SelectPatientsByMaladie(uint32(id_maladie))
		if success {
			c.JSON(200, infections)
		} else {
			c.Writer.WriteHeader(http.StatusNoContent)
		}
	}
}

func GetPrescriptionsByMaladie(c *gin.Context) {
	param_id := c.Param("id")
	id_maladie, err := strconv.ParseInt(param_id, 10, 32)
	if err == nil {
		prescriptions, success := dal.SelectPrescriptionsByMaladie(uint32(id_maladie))
		if success {
			c.JSON(200, prescriptions)
		} else {
			c.Writer.WriteHeader(http.StatusNoContent)
		}
	}
}
