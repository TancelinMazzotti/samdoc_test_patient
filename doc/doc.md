# Dépendance
go get -u github.com/gin-gonic/gin
go get -u github.com/go-oauth2/gin-server
go get -u github.com/dgrijalva/jwt-go
go get -u github.com/tidwall/buntdb

# Base de données
- Créé une base de donnée postgres "samdoc_test"
- Executer le script database.sql pour générer la base
- Executer le script insert.sql pour insérer des données de test

# Configuration des API
- Modifier les fichiers db_config.json par vos informations de connexion à la base de donnée
- Avec postman importer le fichier samdoc_test.postman_collection.json

# Lancement des servers
- Se rendre à la racine du projet
- Taper la commande: go run main.go

# Documentation
Un readme est diponnible à la racine de chaque projet avec le detail des routes
