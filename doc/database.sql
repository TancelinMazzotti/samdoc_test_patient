CREATE EXTENSION pgcrypto;

CREATE TABLE civilite
(
    id_civilite SERIAL,
    libelle VARCHAR(32),
    CONSTRAINT PK_CIVILITE PRIMARY KEY(id_civilite)
);

CREATE TABLE maladie
(
    id_maladie SERIAL,
    libelle VARCHAR(128),
    DESCRIPTION VARCHAR(256),
    CONSTRAINT PK_MALADIE PRIMARY KEY(id_maladie)
);

CREATE TABLE medicament
(
    id_medicament SERIAL,
    libelle VARCHAR(128),
    DESCRIPTION VARCHAR(256),
    CONSTRAINT PK_MEDICAMENT PRIMARY KEY(id_medicament)
);

CREATE TABLE medecin
(
    id_medecin SERIAL,
    nom varchar(32),
    prenom varchar(32),
    mail varchar(128),
    password VARCHAR(256)
);

CREATE TABLE patient
(
    id_patient SERIAL,
    nom VARCHAR(32),
    prenom VARCHAR(32),
    id_civilite INTEGER,
    naissance DATE,
    CONSTRAINT PK_PARIENT PRIMARY KEY(id_patient),
    CONSTRAINT FK_PATIENT_CIVILITE FOREIGN KEY (id_civilite)
        REFERENCES civilite(id_civilite)
);

CREATE TABLE historique
(
    date_infection DATE,
    id_patient INTEGER,
    id_maladie INTEGER,
    CONSTRAINT PK_HISTORIQUE PRIMARY KEY (date_infection, id_patient, id_maladie),
    CONSTRAINT FK_HISTORIQUE_PATIENT FOREIGN KEY (id_patient)
        REFERENCES patient(id_patient),
    CONSTRAINT FK_HISTORIQUE_MALADIE FOREIGN KEY (id_maladie)
        REFERENCES maladie(id_maladie)

);

CREATE TABLE prescription
(
    id_prescription SERIAL,
    id_patient INTEGER,
    id_maladie INTEGER,
    date_infection DATE,
    detail VARCHAR(256),
    CONSTRAINT PK_PRESCRIPTION PRIMARY KEY(id_prescription),
    CONSTRAINT FK_PRESCRIPTION_HISTORIQUE FOREIGN KEY (id_patient, id_maladie, date_infection)
        REFERENCES historique(id_patient, id_maladie, date_infection)
);

CREATE TABLE line_prescription
(
    id_prescription INTEGER,
    id_medicament INTEGER,
    detail VARCHAR(256),
    CONSTRAINT PK_LINE_PRESCRIPTION PRIMARY KEY(id_prescription, id_medicament),
    CONSTRAINT FK_LINE_PRESCRIPTION_PRESCRIPTION FOREIGN KEY (id_prescription)
        REFERENCES prescription(id_prescription),
    CONSTRAINT FK_LINE_PRESCRIPTION_MEDICAMENT FOREIGN KEY (id_medicament)
        REFERENCES medicament(id_medicament)
);
