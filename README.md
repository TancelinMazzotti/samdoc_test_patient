# Dépendance
go get -u github.com/gin-gonic/gin
go get -u github.com/go-oauth2/gin-server
go get -u github.com/dgrijalva/jwt-go
go get -u github.com/tidwall/buntdb


# Configuration
base de donnée: db_config.json
port: 8080


# [POST] /oauth2/token
Description: Route authentification un compte est créé dans les inserts bdd (login: samdoc@gmail.com password: samdoc)
Post form data:
- grant_type = client_credentials
- client_id = samdoc@gmail.com
- client_secret = samdoc
- scope = read

Exemple resultat:
```
{
    "access_token": "AID3OPE-NS-N8F04PQ0MSW",
    "expires_in": 7200,
    "scope": "read",
    "token_type": "Bearer"
}
```

# Route patient
## [GET] /private/patients
Description: Récupérer tout les patients

Exemple resultat:
```
[
    {
        "Id_patient": 60,
        "Nom": "Barber",
        "Prenom": "Palmer",
        "Civilite": "Homme",
        "Naissance": "1957-04-30T00:00:00Z"
    },
    {
        "Id_patient": 23,
        "Nom": "Barlow",
        "Prenom": "Keiko",
        "Civilite": "Femme",
        "Naissance": "1980-09-16T00:00:00Z"
    },
    {
        "Id_patient": 55,
        "Nom": "Bennett",
        "Prenom": "Otto",
        "Civilite": "Homme",
        "Naissance": "1974-06-12T00:00:00Z"
    },
    ...
]
```

## [GET] /private/patient/:id
Description: Récupérer patient
- :id -> identifiant d'un patient

Exemple resultat:
```
{
    "Id_patient": 2,
    "Nom": "Page",
    "Prenom": "Kaye",
    "Civilite": "Homme",
    "Naissance": "1978-06-04T00:00:00Z"
}
```

## [GET] /private/patient/:id/maladies
Description: Récupérer les maladies d'un patient
- :id -> identifiant d'un patient

Exemple resultat:
```
[
    {
        "Date_infection": "1945-09-18T00:00:00Z",
        "Maladie": {
            "Id_maladie": 10,
            "Libelle": "Cancer du poumon",
            "Description": ""
        }
    },
    {
        "Date_infection": "1973-07-06T00:00:00Z",
        "Maladie": {
            "Id_maladie": 54,
            "Libelle": "Allergies",
            "Description": ""
        }
    },
    {
        "Date_infection": "1987-06-02T00:00:00Z",
        "Maladie": {
            "Id_maladie": 48,
            "Libelle": "Lombalgie",
            "Description": ""
        }
    },
    {
        "Date_infection": "2016-05-30T00:00:00Z",
        "Maladie": {
            "Id_maladie": 59,
            "Libelle": "Hépatites",
            "Description": ""
        }
    }
]
```

## [GET] /private/patient/:id/prescriptions
Description: Récupérer les prescriptions d'un patient
- :id -> identifiant d'un patient

Exemple resultat:
```
[
    {
        "Id_prescription": 16,
        "Patient": {
            "Id_patient": 7,
            "Nom": "Simon",
            "Prenom": "Wang",
            "Civilite": "Femme",
            "Naissance": "2019-09-09T00:00:00Z"
        },
        "Maladie": {
            "Id_maladie": 42,
            "Libelle": "Psoriasis",
            "Description": ""
        },
        "Date_infection": "1993-09-29T00:00:00Z",
        "Medicaments": [
            {
                "Id_medicament": 34,
                "Libelle": "PIVALONE",
                "Description": ""
            }
        ],
        "Detail": ""
    },
    {
        "Id_prescription": 6,
        "Patient": {
            "Id_patient": 7,
            "Nom": "Simon",
            "Prenom": "Wang",
            "Civilite": "Femme",
            "Naissance": "2019-09-09T00:00:00Z"
        },
        "Maladie": {
            "Id_maladie": 3,
            "Libelle": "Hypertension",
            "Description": ""
        },
        "Date_infection": "2014-11-21T00:00:00Z",
        "Medicaments": [],
        "Detail": ""
    }
]
```

## [POST] /private/patient
Description: Insert un patient
Paramètre form data:
- nom -> nom du patient
- prenom -> prénom du patient
- id_civilite -> id_corespondant à la civilite (1 = homme, 2 = femme)
- naissance -> date de naissance


# Route maladie
## [GET] /private/maladies
Description: Récupérer tout les patients

Exemple resultat:
```
[
    {
        "Id_maladie": 38,
        "Libelle": "Acné",
        "Description": ""
    },
    {
        "Id_maladie": 68,
        "Libelle": "Alcoolisme",
        "Description": ""
    },
    {
        "Id_maladie": 54,
        "Libelle": "Allergies",
        "Description": ""
    },
    ...
]
```

## [GET] /private/maladie/:id
Description: Récupérer maladie
- :id -> identifiant de la maladie

Exemple resultat:
```
{
    "Id_maladie": 68,
    "Libelle": "Alcoolisme",
    "Description": ""
}
```


## [GET] /private/maladie/:id/patients
Description: Récupérer les patients infecter par une maladie
- :id -> identifiant de la maladie

Exemple resultat:
```
[
    {
        "Date_infection": "1945-10-01T00:00:00Z",
        "Patient": {
            "Id_patient": 86,
            "Nom": "Martin",
            "Prenom": "Berk",
            "Civilite": "Femme",
            "Naissance": "1959-11-19T00:00:00Z"
        }
    },
    {
        "Date_infection": "1973-09-25T00:00:00Z",
        "Patient": {
            "Id_patient": 95,
            "Nom": "Hewitt",
            "Prenom": "Serena",
            "Civilite": "Femme",
            "Naissance": "2015-06-22T00:00:00Z"
        }
    },
    ...
]
```

## [GET] /private/maladie/:id/prescriptions
Description: Récupérer les prescriptions concernant une maladie
- :id -> identifiant de la maladie

Exemple resultat:
```
[
    {
        "Id_prescription": 136,
        "Patient": {
            "Id_patient": 92,
            "Nom": "Michael",
            "Prenom": "Ahmed",
            "Civilite": "Femme",
            "Naissance": "1957-03-27T00:00:00Z"
        },
        "Maladie": {
            "Id_maladie": 2,
            "Libelle": "Cholestérol",
            "Description": ""
        },
        "Date_infection": "1954-12-17T00:00:00Z",
        "Medicaments": [
            {
                "Id_medicament": 48,
                "Libelle": "VENTOLINE",
                "Description": ""
            }
        ],
        "Detail": ""
    }
]
```

# Route prescription
## [GET] /private/prescriptions
Description: Récupérer toute les prescriptions

Exemple resultat:
```
[
    {
        "Id_prescription": 10,
        "Patient": {
            "Id_patient": 6,
            "Nom": "Meyer",
            "Prenom": "Lillian",
            "Civilite": "Homme",
            "Naissance": "1993-06-10T00:00:00Z"
        },
        "Maladie": {
            "Id_maladie": 10,
            "Libelle": "Cancer du poumon",
            "Description": ""
        },
        "Date_infection": "1945-09-18T00:00:00Z",
        "Medicaments": [
            {
                "Id_medicament": 74,
                "Libelle": "APROVEL",
                "Description": ""
            }
        ],
        "Detail": ""
    },
    {
        "Id_prescription": 4,
        "Patient": {
            "Id_patient": 86,
            "Nom": "Martin",
            "Prenom": "Berk",
            "Civilite": "Femme",
            "Naissance": "1959-11-19T00:00:00Z"
        },
        "Maladie": {
            "Id_maladie": 68,
            "Libelle": "Alcoolisme",
            "Description": ""
        },
        "Date_infection": "1945-10-01T00:00:00Z",
        "Medicaments": [
            {
                "Id_medicament": 37,
                "Libelle": "EUPANTOL",
                "Description": ""
            }
        ],
        "Detail": ""
    },
    ...
]
```

## [GET] /private/prescription/:id
Description: Récupérer prescription
- :id -> identifiant de la prescription

Exemple resultat:
```
{
    "Id_prescription": 3,
    "Patient": {
        "Id_patient": 68,
        "Nom": "Kramer",
        "Prenom": "Jennifer",
        "Civilite": "Femme",
        "Naissance": "2016-06-10T00:00:00Z"
    },
    "Maladie": {
        "Id_maladie": 51,
        "Libelle": "Rhumatismes",
        "Description": ""
    },
    "Date_infection": "1983-11-19T00:00:00Z",
    "Medicaments": [
        {
            "Id_medicament": 88,
            "Libelle": "ART",
            "Description": ""
        },
        {
            "Id_medicament": 15,
            "Libelle": "FORLAX",
            "Description": ""
        }
    ],
    "Detail": ""
}
```


## [GET] private/prescriptions/patient/:idPatient/maladie/:idMaladie
Description: Récupérer prescriptions pour un client et une maladie
- :id -> identifiant de la prescription

Exemple resultat:
```
[
    {
        "Id_prescription": 3,
        "Patient": {
            "Id_patient": 68,
            "Nom": "Kramer",
            "Prenom": "Jennifer",
            "Civilite": "Femme",
            "Naissance": "2016-06-10T00:00:00Z"
        },
        "Maladie": {
            "Id_maladie": 51,
            "Libelle": "Rhumatismes",
            "Description": ""
        },
        "Date_infection": "1983-11-19T00:00:00Z",
        "Medicaments": [
            {
                "Id_medicament": 88,
                "Libelle": "ART",
                "Description": ""
            },
            {
                "Id_medicament": 15,
                "Libelle": "FORLAX",
                "Description": ""
            }
        ],
        "Detail": ""
    }
]
```


# Route medicament
## [GET] /private/medicaments
Description: Récupérer tout les medicaments

Exemple resultat:
```
[
    {
        "Id_medicament": 36,
        "Libelle": "ADVIL",
        "Description": ""
    },
    {
        "Id_medicament": 28,
        "Libelle": "AERIUS",
        "Description": ""
    },
    {
        "Id_medicament": 81,
        "Libelle": "ALODONT",
        "Description": ""
    },
    ...
]
```

## [GET] /private/medicament/:id
Description: Récupérer médicament
- :id -> identifiant du medicament

Exemple resultat:
```
{
    "Id_medicament": 3,
    "Libelle": "DAFALGAN",
    "Description": ""
}
```